package Principal;

import java.util.ArrayList;

public class SingletonTurma {
	
	private static ArrayList<Turma> turmas = null;
	
	public static ArrayList<Turma> getInstance(){
		if(turmas == null)turmas = new ArrayList<Turma>();
		return turmas;
	}
}
