package Principal;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Composite.Curso;
import Composite.Modulo;

public class Principal {
	
	private List<Modulo> modulos = new ArrayList<>();
	private Scanner s = new Scanner(System.in);
	
	public Principal() {
		System.out.println("Bem Vindo ao composite");
		
		while(true){
			System.out.println("1-Inscri��o de aluno\n"
					+ "2-Criar curso\n"
					+ "3-Criar modulo\n"
					+ "4-Criar turma\n"
					+ "5-listar turmas"
					+ "6-Sair\n"
					+ "Op��o: ");
			String op = s.next();
			switch(op){
				case "1": inscricaoAluno(); break;
				case "2": criarCurso(); break;
				case "3": criarModulo(); break;
				case "4": montarTurmas(); break;
				case "5": listarTurmas(); break;
				case "6": System.exit(1); break;
				default: continue;
			}
		}
		
		
	}
	
	private void listarTurmas(){
		for (int i = 0; i < SingletonTurma.getInstance().size(); i++) {
			System.out.println("Nome da turma: "+SingletonTurma.getInstance().get(i).getCode());
			SingletonTurma.getInstance().get(i).listarAlunos();
			System.out.println("Sala: "+SingletonTurma.getInstance().get(i).getSala());
		}
	}
	
	private void montarTurmas(){
		System.out.println("Montando turmas...");
		Turma t = new Turma();
		for (int i = 0; i < modulos.size(); i++) {
			modulos.get(i).abrirTurma(t.clone());
		}
		System.out.println("Turmas abertas");
		listarTurmas();
	}
	
	private void inscricaoAluno(){
		System.out.println("Nome do aluno: ");
		String nome = s.next();
		listarModulos();
		System.out.println("inscrive-lo no modulo: ");
		int k = Integer.parseInt(s.next());
		modulos.get(k).addAluno(new Aluno(nome));
		System.out.println("Inicrito com sucesso!");
	}
	
	private void criarModulo(){
		System.out.println("Nome do novo modulo: ");
		String nome = s.next();
		System.out.println("Quantidade de alunos minimas para iniciar modulo: ");
		int min = Integer.parseInt(s.next());
		System.out.println("Carga horaria do modulo: ");
		int carg = Integer.parseInt(s.next());
		System.out.println("Valor do modulo: ");
		float valor = Float.parseFloat(s.next());
		modulos.add(new Modulo(nome, min, carg, valor));
	}
	
	private void criarCurso(){
		System.out.println("Nome do curso");
		String nome = s.next();
		listarModulos();
		System.out.println("Entre com o modulos do curso separados por virgula: ");
		String ops = s.next();
		ArrayList<Integer> lista = getListaDeInt(ops);
		addNoCursoModulos(nome, lista);
	}
	
	private void addNoCursoModulos(String nome, ArrayList<Integer> lista){
		Curso c = new Curso(nome);
		for (int i = 0; i < lista.size(); i++) {
			c.addModulo(modulos.get(lista.get(i)));
		}
		modulos.add(c);
		System.out.println("Curso criado com sucesso!");
	}
	
	private ArrayList<Integer> getListaDeInt(String op){
		ArrayList<Integer> lista = new ArrayList<Integer>();
		int  k = 0;
		for (int i = 0; i < op.length(); i++) {
			if(op.charAt(i) == ','){
				lista.add(Integer.parseInt(op.substring(k, i)));
				k = i + 1;
			}
		}
		lista.add(Integer.parseInt(op.substring(k)));
		return lista;
	}
	
	private void listarModulos(){
		for (int i = 0; i < modulos.size(); i++) {
			System.out.println(i+"-"+modulos.get(i).getTitulo());
			
		}
	}
}

