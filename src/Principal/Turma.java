package Principal;

import java.io.Serializable;
import java.util.ArrayList;

public class Turma implements Cloneable{
	
	private String sala;
	private String code;
	private ArrayList<Aluno> alunos;
	
	public Turma() {
		alunos = new ArrayList<Aluno>();
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public void setSala(String sala) {
		this.sala = sala;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getSala() {
		return sala;
	}
	
	public void addAluno(Aluno a){
		alunos.add(a);
	}
	
	@Override
	public Turma clone(){
		try {
			Turma t = (Turma) super.clone();
			t.alunos = new ArrayList<>();
			return t;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void listarAlunos(){
		for (int i = 0; i < alunos.size(); i++) {
			System.out.println((i+1)+"-"+alunos.get(i).getNome());
		}
	}
}
