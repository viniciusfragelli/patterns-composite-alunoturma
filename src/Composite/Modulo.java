package Composite;

import java.util.ArrayList;
import java.util.List;

import Principal.Aluno;
import Principal.SingletonSala;
import Principal.SingletonTurma;
import Principal.Turma;

public class Modulo {
	
	protected String titulo;
	protected int cargahoraria;
	protected int minaluno;
	protected float valor;
	protected List<Aluno> alunos;
	
	public Modulo(String titulo, int minaluno, int cargahoraria, float valor) {
		alunos = new ArrayList<Aluno>();
		setValor(valor);
		setTitulo(titulo);
		setCargahoraria(cargahoraria);
		setMinaluno(minaluno);
	}
	
	public void addAluno(Aluno a){
		alunos.add(a);
	}
	
	public Modulo() {
		// TODO Auto-generated constructor stub
	}
	
	public void abrirTurma(Turma t) {
		if(alunos.size() >= minaluno){
			int salas = alunos.size()/minaluno;
			int k = 0, j = 0;
			for (int i = 0; i < salas; i++) {
				Turma t1 = t.clone();
				t1.setCode(getTitulo()+""+i);
				t1.setSala(SingletonSala.getSala()+"");
				for (j = k; j < minaluno+k; j++){
					t1.addAluno(alunos.get(j));
				}
				k += minaluno;
				SingletonTurma.getInstance().add(t1);
			}
			for (int i = 0; i < salas*minaluno;i++) {
				alunos.remove(0);
			}
		}
	}
	
	public ArrayList<Modulo> getListaDeModulos(){
		return null;
	}
	
	public void setValor(float valor) {
		this.valor = valor;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public void setMinaluno(int minaluno) {
		this.minaluno = minaluno;
	}
	
	public void setCargahoraria(int cargahoraria) {
		this.cargahoraria = cargahoraria;
	}
	
	public float getValor() {
		return valor;
	}
	
	public String getTitulo() {
		return titulo;
	}
	
	public int getMinaluno() {
		return minaluno;
	}
	
	public int getCargahoraria() {
		return cargahoraria;
	}
	
	public void addModulo(Modulo m){
		
	}
}
