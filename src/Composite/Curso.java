package Composite;

import java.util.ArrayList;

import Principal.Aluno;
import Principal.Turma;

public class Curso extends Modulo{

	private ArrayList<Modulo> modulos;	
	
	public Curso(String nome) {
		super();
		modulos = new ArrayList<>();
		setTitulo(nome);
	}
	
	@Override
	public void addModulo(Modulo m){
		/*if(m instanceof Curso){
			ArrayList<Modulo> mod = m.getListaDeModulos();
			for (int i = 0; i < array.length; i++) {
				modulos.add
			}
		}else{*/
			modulos.add(m);
		//}
	}
	
	@Override
	public int getCargahoraria() {
		int total = 0;
		for (int i = 0; i < modulos.size(); i++) {
			total += modulos.get(i).getCargahoraria();
		}
		return total;
	}
	
	@Override
	public int getMinaluno() {
		int total = 0;
		for (int i = 0; i < modulos.size(); i++) {
			total += modulos.get(i).getMinaluno();
		}
		return total;
	}
	
	@Override
	public String getTitulo() {
		return super.getTitulo();
	}
	
	@Override
	public float getValor() {
		float total = 0;
		for (int i = 0; i < modulos.size(); i++) {
			total += modulos.get(i).getValor();
		}
		return total;
	}
	
	public String getModulos() {
		String total = "";
		for (int i = 0; i < modulos.size(); i++) {
			total += modulos.get(i).getTitulo()+"\n";
		}
		return total;
	}
	
	@Override
	public ArrayList<Modulo> getListaDeModulos(){
		return modulos;
	}
	
	@Override
	public void addAluno(Aluno a) {
		for (int i = 0; i < modulos.size(); i++) {
			modulos.get(i).addAluno(a);
		}
	}
	
	@Override
	public void abrirTurma(Turma t) {
		
	}
}
